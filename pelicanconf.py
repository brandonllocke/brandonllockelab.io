#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Brandon L. Locke'
SITENAME = 'brandonllocke'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'America/New_York'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('About Me', '/pages/about-me.html'),
        ('Categories', '/categories.html'),
        ('Tags', '/tags.html'),
        ('Archive', '/archives.html'),)

MENUITEMS = (('About Me', '/pages/about-me.html'),
        ('Categories', '/categories.html'),
        ('Tags', '/tags.html'),
        ('Archive', '/archives.html'),)

# Social widget
SOCIAL = (('twitter', 'https://twitter.com/brandonllocke'),
          ('linkedin', 'https://linkedin.com/in/brandonllocke'),
          ('github', 'https://github.com/brandonllocke'),)

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'pelican-bootstrap3'
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
PLUGIN_PATHS = ['/i18n_subsites']
PLUGINS = ['i18n_subsites',
           'frontmark']
DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = False
#DISPLAY_CATEGORIES_ON_SIDEBAR = True
#DISPLAY_TAGS_ON_SIDEBAR = True
#HIDE_SIDEBAR = True
PADDED_SINGLE_COLUMN_STYLE = True
DEFAULT_PAGINATION = 1
