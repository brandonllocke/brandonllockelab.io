---
Title: Windows Ain't That Bad...
Tags: software, linux, windows
Category: Rant
---

I have to admit it. Windows isn't that bad... provided that you use it sparingly, have Window's Subsystem for Linux installed, and can deal with awful desktop paradigms.

## Use Sparingly

> "How did Windows become the default for computers?"

I'm not sure I've ever been more proud than when one of my younger employees said this out of complete and utter disgust with Windows. Being a very inexperienced member of the team, we had him working on swapping users' machines around when needed. He had done it a few times on MacOS (via Time Machine) and even once on Linux (via just moving the hard drive). So when we needed to transition a Windows user from one machine to another, he thought, "I'll just grab a backup using Windows Backup and restore the whole thing to the other computer!" I had to break it to him that the computers he was swapping were not the same model or even brand of computers and that he would likely have more issues once that backup was restored than he wanted to deal with. His face fell and he uttered the famous words above.

Unless you buy in completely, you don't really get many of the benefits of the Windows platform. It's inability to easily handle driver and hardware changes make it annoying to move users between machines. It's lack of a solid command line forces us to manage everything from a GUI when a CLI would be much more efficient.

Needless to say, my recommendation is to use Windows sparingly. For instance, in my home, I have one machine running Windows. It's for Steam games... that is all. With that being said, it's not even what we typically use for the games. I'm typically using Steam In-Home Streaming to stream the Windows computer to the many other Linux computers in my house.

## Window's Subsystem for Linux or Bust

The addition of WSL to Windows was pretty interesting. It meant that overnight I could go from "this is uncomfortable to accomplish on Windows" to "I can use all the tricks I normally use to accomplish things on Windows"! It does strike me as a bit odd that the only reason I've really found Windows more tolerable lately is that it includes Linux now. That's not great marketing for Microsoft! However, perhaps if I hadn't already been a Linux user, it would have kept me on Windows far longer seeing as I didn't need to make some of the hurdles required when moving from Windows to Linux. I guess we'll never know!

## Poor Desktop

Windows has made strides on their desktop paradigm. It now has something similar to desktop workspaces. It allows you to easily shift window locations with your keyboard. It even moved towards a "dock" paradigm with the new taskbar of post-7 Windows versions. Yet, it feels like too little too late.

I'm a tiling window manager user (it's pretty close to saying "I use Arch btw..."). It took me a week or two to get over the learning curve, but once I did, it changed the way I used a computer. It just became _easier_ to get things done. Simultaneously, it became _harder_ to work on a computer utilizing a floating window manager. When I sit down behind a coworkers Mac or Windows computer, I feel like both of my arms are tied behind my back. 

>_Why can't I just hover over a window to bring it into focus?_
>_Why does your Caps Lock key turn on your Caps Lock?_ 
>_What's the keyboard shortcut to get shove this window 10 digital desktops away from me while I work on these two windows I want side by side?_
>_Seriously, why is it so hard to put these two windows side by side with another longer window underneath both?_

By now you can guess that I just find the floating window paradigm to be clumsy. I feel the same way about Gnome, KDE, Openbox, Cinnamon, etc. I understand that it's a bit cult-like to say that tiling window managers make floating windows completely obsolete, but it's perhaps more true to say that cars are much more efficient than bicycles, but bicycles are much easier to use.

## The Take Away

Use Windows if you want. I don't care. To poorly paraphrase a Bible verse: "as for me and my house, we'll use Linux". In the end, they are both tools. I feel that Linux is a much more efficient tool for the work that I do. You may feel differently. I might need a jackhammer and you may be fine with a hammer and chisel. Each have their place. Just make sure to learn what you use! _If I hear one my person say "I'm just not good with computers despite using them every day for the last 30 years of my life..." I'm going to lose it._ That's another rant for another time though...
