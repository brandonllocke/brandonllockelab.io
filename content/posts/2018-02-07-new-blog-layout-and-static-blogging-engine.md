---
Title: New Blog Layout and Static Blogging Engine
Tags: thechurchonline, commandline_vs_gui, jekyll, pelican, wordpress, joomla, contains_ideas_for_future_posts
Category: Software
---

### EDIT: Ignore this, I've moved back to Pelican because Python == 'amazing'.

I'm now using [Jekyll](https://jekyllrb.com/) to create my blog. Since I've started working at [The Church Online](http://www.thechurchonline.com), I've seen a lot of dynamic sites at work and I must admit there is a definite draw there. I'm just not into dynamic content on my own personal site for a few main reasons.

## 1. I'm cheap.

First and foremost, I'm cheap. I really don't want to spend a ton of money on a website/blog. Most of you are probably saying *"You know you can get hosting for pennies on the dollar with x/y/z company, right?"* Sure, that is an absolute possibility. On the other hand, you can see from my posting frequency, that my website is sort of waxing and wanning pursuit of mine. Sometimes, I'm really into it. Other times, meh. I'd prefer to keep my costs low.

## 2. I don't really need crazy features (even though they are nice).

We're constantly buying plugins and features to add into our Wordpress and Joomla sites at work and some of them are really compelling. Fortunately, I'm just not in great need of a lot of them right now. The previous version of this blog was simply a list of articles that I'd written with links to the pages that held them. I never used tags, categories, etc. and had no thought in my mind to use anything like comments or whatever new-fangled things the kids come up with now-a-days. I'm starting to use tags and categories now, since it's built right into this platform and I'm getting more comfortable with the technology behind it, but it still feels a bit superfluous to me.

## 3. I like simple things.

The backends of the dynamic sites built on Wordpress, Joomla, and the like are gorgeous and give you a ton of flexibility when writing. It's really not much different than if Microsoft Word (or LibreOffice, if you will) had a giant "publish" button at the end. My issue with it is that I swing back and forth between wanting these nice things laid out for me and wanting to get back to simplicity with just me and my terminal. Jekyll allows me to write posts using [Jekyll-Admin](https://github.com/jekyll/jekyll-admin) or just popping into vim in a terminal and plugging away[^1]. On top of that, I'm messing with databases and all that jazz all day at work. I really don't need to come home and do more of it. Database management is not one of my favorite parts of my work, I'd rather be learning to automate something or configuring a new piece of software that will help me automate something else.

## 4. Text files are archivable.

**I LOVE TEXT FILES.** Seriously, anyone who can't find the joy in a solid txt is out of touch with the beautiful simplicity of it all. The use of text files to configure software is one of the things that drew me to Linux. Text files are so simple and yet so powerful. If tomorrow Jekyll ended development and shut down their site and for some odd reason, the executable stopped working: I'd still have all my text files in Markdown when I wanted to move them to a new platform/site. No mucking around with getting content out of a database or squeezing it through a converter. It's right there. If I ever wanted to archive my posts, I could simply zip up my "posts" folder and be on my way. It would likely be less than a few kilobytes when all was said and done. **I LOVE TEXT FILES.**

Ultimately, it's a personal preference thing. I get far more flexibility, security, and ease-of-use out of a static blogging platform like my previous static blog program, [Pelican](https://blog.getpelican.com) or my current platform, Jekyll than I ever would with Wordpress or Joomla.

[^1]: I should do a post on why I think I swing back and forth between GUI and command line desires and why I almost always end up back in the terminal.
