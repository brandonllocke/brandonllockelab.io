---
Title: Install Syncthing on Debian Jessie or Stretch.
Tags: syncthing, jessie
Category: Tutorials
---

Syncthing is a great tool for liberating your data from cloud storage providers. While Dropbox, SpiderOak, and Google Drive are great services, the trade-off for such a great and simple product is privacy and loss of control. As an example, Copy.com was a great service that had an excellent Linux client and even a headless client for server installs, unfortunately, it was just shut down a few days ago when Baracuda decided to abandon that service. I had it installed on my parent's computer as an alternative backup and completely forgot until my mother sent me a message saying the computer was telling her her data would be deleted. I had long since removed it from my workflow and since I was often just using my laptop, it wasn't necessary. I've reintroduced my desktop to my workflow and soon the need for such software arose again. 

## Installing Syncthing

While Syncthing isn't available in the repos, there is a repo officially run by Syncthing. Add it to your apt sources like this:

	# Add the release PGP keys:
	curl -s https://syncthing.net/release-key.txt | sudo apt-key add -
	
	# Add the "release" channel to your APT sources:
	echo "deb http://apt.syncthing.net/ syncthing release" | sudo tee /etc/apt/sources.list.d/syncthing.list

Then update your repos and install syncthing:

	sudo apt-get update
	sudo apt-get install syncthing

Note: doing this on one machine only won't do much of anything. There is nothing to sync with,so you'll want to do this on each computer you want to sync.

Now that the application is installed, let's run it!

	syncthing

Open up your browser and go to:

	https://localhost:8384

You'll be greeted with a nice web interface where you can find your ID and add other "devices" using their IDs. Once you do that, you can add folders and share them with your devices. The web interface is very intuative and easy to use.

## Take Away

I've been using it for a few days now and it works very well! It really only works if you have a machine that will remain on all the time. I use my htpc/server as a main hub and then it populates my laptop and desktop with changes made from each other. At first, I thought I was missing the web interface that allows me to grab a single file from anywhere. The truth is, there are still a ton of ways to do that. I could use ftp, scp, or even http, if I wanted to set it up and secure it. I just don't really need it right now. I can just pull it down with my phone over ssh.
