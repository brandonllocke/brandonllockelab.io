---
Title: Journey Into Python
Tags: python, new_endeavor, apis
Category: Programming
---

So, I've been teaching myself Python lately. This is partly due to work and partly due to wanting to do this for a few years now. Pretty quickly after I moved to Linux full time many years ago, the whole concept of programming and scripting really excited me. I quickly learned that with bash, I could make the computer do what would normally take me multiple hours of clicking here and clicking there and still not getting done.

## Python for Work

At work, Python has come in handy with a number of projects that I've had added to my To Do List. We use a project management system that has a decent RESTful API. From there, I'm able to get lists of all projects in a certain state, all projects that have entered that state since the last time we checked it, etc. I have a whole set of scripts worked up for multiple reasons. Beyond our project management stuff, I've been using Python to work with our web hosting stuff, video encoding boxes, and multiple other applications.

## Python for Me

I've been writing Python scripts for a few different reasons outside of work. First, I use a music streaming software that features an API and I've been working on a terminal based client for it. Right now, it's pretty much useless, it just downloads songs based on a pre-selected list, but eventually, I want to build out a ncurses interface for it.
