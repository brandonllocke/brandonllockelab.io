---
Title: Using ddclient on the Raspberry Pi.
Tags: ddclient, raspberry-pi
Category: Tutorials
---

The Raspberry Pi is anything but a powerhouse, but it excels in a lot of lightweight applications that rely on 24/7 uptime. Its power-sipping processor makes it amazing for simple things like acting as an ssh gateway for your home network or hosting any number of network applications from inside your home network. Today, I'll go over my process to set up ddclient on the Raspberry Pi to automatically update a domain that I have purchased with the external ip of my network.

**Note: This assumes you have a working Raspberry Pi already on your network. Mine is running Raspbian Jessie. The domain is registered at NameCheap. This tutorial should work with almost any domain registrar or distribution, with some minor changes you'll have to make yourself.**

First, install ddclient on your Raspberry Pi:
    
	sudo apt install ddclient

Second, create a config file on your Raspberry Pi:
    
	sudo touch /etc/ddclient.conf

Third, open the file in your favorite editor (mine is vim):

	sudo vim /etc/ddclient.conf

Fourth, plug in the relevant information according to this template:

    # Configuration file for ddclient
    #
    # /etc/ddclient.conf

	use=web, web=dynamicdns.park-your-domain.com/getip
	protocol=namecheap
	server=dynamicdns.park-your-domain.com
	login=*your_domain*
	password=*the password you get from the dyndns section of the namecheap website*
	*subdomain*

To clarify, if I wanted to make raspberrypi.brandonllocke.com resolve to my RPi, my config would look like this:

    # Configuration file for ddclient
    #
    # /etc/ddclient.conf

	use=web, web=dynamicdns.park-your-domain.com/getip
	protocol=namecheap
	server=dynamicdns.park-your-domain.com
	login=brandonllocke.com
	password=*the password you get from the dyndns section of the namecheap website*
	raspberrypi

This will allow you to update the ip address manually, however, I use Linux because it allows me to automate things easily. Therefore, lets daemonize this whole thing and let it run every so often to auto update the ip address whenever my ISP decides to change it.

First, create a file in /etc/default

    sudo touch /etc/default/ddclient

Second, plug in the relevant information according to this template:

	# Configuration for ddclient scripts 
	#
	# /etc/default/ddclient
	
	# Set to "true" if ddclient should be run every time DHCP client ('dhclient'
	# from package isc-dhcp-client) updates the systems IP address.
	run_dhclient="false"
	
	# Set to "true" if ddclient should be run every time a new ppp connection is 
	# established. This might be useful, if you are using dial-on-demand.
	run_ipup="true"
	
	# Set to "true" if ddclient should run in daemon mode
	# If this is changed to true, run_ipup and run_dhclient must be set to false.
	run_daemon="true"
	
	# Set the time interval between the updates of the dynamic DNS name in seconds.
	# This option only takes effect if the ddclient runs in daemon mode.
	daemon_interval="300"

You can set the interval to anything you might want. I would recommend not setting it super low, as this can hammer online resources and that's a really good way to upset people and push resources offline.

You can start ddclient's daemon with the following command:

    sudo service ddclient start

It should start up automatically on reboot, but after you reboot you can check on the status of the service by running:

    sudo service ddlclient status

You should be set! I hope this helped!