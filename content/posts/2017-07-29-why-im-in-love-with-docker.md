---
Title: Why I'm In Love With Docker.
Tags: fanboy, docker, wordpress, joomla, mysql, containers
Category: Rants
---

Docker has been on the scene for a while now. It's being used to simplify workflows all the way from the first steps of development through to launch and onward with support and upgrades. I've been messing with Docker for roughly a year or so now and I have to admit that I am in love. It's changed the way that I do a lot of software testing and deployment, both at home and at work. The reason for this shift in my normal workflow is five fold.

## 1. Docker makes software testing simple.

Back in the day, installing and configuring new software was a matter of reading docs, changing config files, and ensure dependencies were all accounted for. Docker wraps up a lot of that into a quick and easy script called a Dockerfile. It installs dependencies, sets default configs (although main options are often editable), and gets everything up and running without a great deal of hassle.

Beyond this, due to Docker's "containerization" feature, Docker doesn't spread multiple version of libraries, programs, etc. all over your filesystem.

All of this contributes to Docker's ability to make software testing simple. I love documentation and config editing as much as anyone else, but first and foremost, I want to see if the software has the features, abilities, and interface that benefits my users. Essentially, let's just get it up and running and run some tests and if it seems like something I want to pursue further, I'll dive further into the docs and setting it up for my specific use-case.


## 2. Docker performs the same everywhere.

One of the issues that I come up against often is that software reacts differently on different platforms. Varying version of this or that library mean that the website that worked beautifully on one server won't even start on another. Again, Docker's containerization feature makes this a thing of the past. Because each container acts like a self-contained unit, it works the same anywhere Docker can be installed. I used to have to use full fledged virtual machines to get this type of "work anywhere" functionality. Now I can do the same without all the wasted overhead.


## 3. Docker relies on already established skills and tools.

In a seemingly contradictory statement to the one in the first section, Docker allows me to use my previously learned skills of reading and understanding documentation, editing configuration files, and installing dependencies in a way that makes Docker completely custom.

Like I said, while I'm testing software, I just want it up and running. Once I have done my due diligence regarding the software, I want to be able to tweak and change almost everything about it to make it perfect. I can do that with my already existing knowledge of the Linux sub-system.

## 4. Docker increases system flexibility.

Normally, services will share the same instance of a program when they also share a server. Got a Wordpress and Joomla install on the same server? Chances are they are both using the same database server with separate databases. This is great when everything needs the same version, but what happens when they need different versions? What happens when you need to update a version for one application but it's incompatible with the other application? Docker allows you to run different versions of the same software without problems or extra work. Tied into that is number 5.

## 5. Docker increases system uptime.

So we just found out that we can run separate instances of the same application in different versions. This can actually increase system uptime in general. Using our previous example, you have a Joomla instance and a Wordpress instance both using the same MySQL instance. Well, if Joomla does something and MySQL ends up dying, it kills not only your Joomla instance but also your Wordpress instance. With Docker, Joomla and Wordpress have their own MySQL containers. If the Joomla instance of MySQL dies, you'll still have downtime on the Joomla site, but it won't kill your Wordpress application at the same time. And just in case someone was worried that they have to watch so many different services now, Docker has built in tools to automatically notify you if something does "die" on it's own. They can even restart on their own on failure!

Docker is pretty amazing and I'm fairly certain it will be a tool I use on almost a daily basis.